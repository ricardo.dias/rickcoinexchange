$(document).ready ->

  requestConvert = () ->
      $.ajax '/convert',
          type: 'GET'
          dataType: 'json'
          data: {
                  source_currency: $("#source_currency").val(),
                  target_currency: $("#target_currency").val(),
                  amount: $("#amount").val()
                }
          error: (jqXHR, textStatus, errorThrown) ->
            alert textStatus
          success: (data, text, jqXHR) ->
            $('#result').val(data.value)
        return false;

      verifyMinValue = () ->
        if $("#amount").val() < 1
          alert('Favor inserir um valor positivo.')
          $("#amount").val(1)

    $ '#amount, #source_currency, #target_currency'
      .on "keyup change", ->
        verifyMinValue()
        requestConvert()

    $ '#btn-inverter'
      .click (e) ->
        source_currency_now = $("#source_currency").val()
        target_currency_now = $("#target_currency").val()
        $("#source_currency").val(target_currency_now)
        $("#target_currency").val(source_currency_now)
        requestConvert()